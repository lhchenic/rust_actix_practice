use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder};

mod fibonacci;

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(hello)
            // .service(echo)
            .route("/fib", web::get().to(fibonacci::print_fibonacci_qstring))
    })
    .bind(("127.0.0.1", 80))?
    .run()
    .await
}
