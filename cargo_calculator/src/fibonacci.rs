extern crate cpython;
extern crate qstring;

use actix_web::{
    // error,
    // http::{header::ContentType, StatusCode},
    HttpRequest,
    HttpResponse,
};
use cpython::{GILGuard, ObjectProtocol, PyDict, PyModule, PyResult, Python};
use qstring::QString;

const FIBO_PY: &'static str = include_str!("../python/fibonacci.py");

pub async fn print_fibonacci_qstring(req: HttpRequest) -> HttpResponse {
    // Declare python interpreter.
    let gil: GILGuard = Python::acquire_gil();
    let py: Python<'_> = gil.python();

    /* https://stackoverflow.com/a/58055305 */
    // Parse url string and get argument.
    let query: &str = req.query_string();
    let qs: QString = QString::from(query);
    let target: i32 = qs.get("target").unwrap().parse::<i32>().unwrap();

    // Handle overflow.
    if target > 30 {
        HttpResponse::Ok().body("Only number <= 30 is allowed.")
    } else {
        // Call python module to compute Fibonacci numbers.
        let _ans = example_fibonacci(py, target);
        HttpResponse::Ok().json(_ans.await.unwrap())
    }
}

pub async fn example_fibonacci(py: Python<'_>, target: i32) -> PyResult<Vec<i32>> {
    let m: PyModule = module_from_str(py, "fibonacci", FIBO_PY).await?;
    let ans: Vec<i32> = m
        .call(py, "fibonacci_numbers", (target,), None)?
        .extract(py)?;

    Ok(ans)
}

/// https://github.com/dgrunwald/rust-cpython/issues/121
///
/// Import a module from the given file contents.
///
/// This is a wrapper around `PyModule::new` and `Python::run` which simulates
/// the behavior of the builtin function `exec`. `name` will be used as the
/// module's `__name__`, but is not otherwise important (it does not need
/// to match the file's name).

pub async fn module_from_str(py: Python<'_>, name: &str, source: &str) -> PyResult<PyModule> {
    let m: PyModule = PyModule::new(py, name)?;
    m.add(py, "__builtins__", py.import("builtins")?)?;
    let m_locals: PyDict = m.get(py, "__dict__")?.extract(py)?;

    // To avoid multiple import, and to add entry to the cache in `sys.modules`.
    let sys: PyModule = cpython::PyModule::import(py, "sys").unwrap();
    sys.get(py, "modules")
        .unwrap()
        .set_item(py, name, &m)
        .unwrap();

    // Finally, run the module
    py.run(source, Some(&m_locals), None)?;
    Ok(m)
}
