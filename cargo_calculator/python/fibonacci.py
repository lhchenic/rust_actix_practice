def fibonacci_numbers(n: int = 2):
    """
    Return Fibonacci series up to n numbers,
    where n >= 2.
    """

    result = [1, 1]
    while len(result) < n:
        result.append(result[-2] + result[-1])

    return result
